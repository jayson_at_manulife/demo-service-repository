# Change Log

## Version: 0.0.1

Date of Release: 26 December 2017

Release Notes:

Initial version
Support for service registry, config server, app autoscaler, new relic
Swagger maven plugin integration
​​

## Version: 0.0.2

Date of Release: 10th January 2018

Release Notes:

Support for Swagger delegates
Support for spring-security using OAuth 2.0
Support for caching
Standardized configuration files
Support for maven and spring profiles
Added name of app in service in maven pom banner
Remove bugs in logging, swagger​

## Version: 0.0.3

Date of Release: 13th February 2018

Release Notes:

Add support for Sonarqube linked to Asia ​Sonarqube instance
Add support for PMD & CPD
Add support to build site information
Add Hystrix Dashboard service in manifest files
Remov​ed support for Gradle build
Improved documentation for README.md, CONTRIBUTORS.md, CHANGELOG.md
Simplify code for generators.
Update swagger-input.yml template
Bugs & issues reported.​​


## Version: 0.0.4

Date of Release: 21st March 2018

Release Notes:

Swagger generated documentation support.
Maven site plugin support for project information & reports (pmd, source xref, test source xref, code coverage, surefire report, checkstyle).
Added developer and contributors tags in pom.xml
Update manifest files to remove newrelic and scale.
Bugs & issues reported.​​